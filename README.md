# Twiiter Tools

Twitter management tools to block bots and hoaxes accounts easily and perform batch actions.

## Requirements

- Twitter developer account
- Python3
- Node.js
- npm

## Installation

Clone the proyect from the repository:

```bash
git clone https://zepo87@bitbucket.org/zepo87/twitter-tools.git
```

Install python requirements:

```bash
pip install -r requirements.txt
```

Populate .env file with your own credentials:
```
API_KEY=
API_SECRET=
TOKEN=
TOKEN_SECRET=
```

Install the frontend (in app/frontend):

```bash
npm install
```

Run build the frontend:

```bash
npm run build
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)